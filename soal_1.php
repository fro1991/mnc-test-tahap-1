
<?php
$strings = array ('abcd','acbd','abcd','acbd'); // Input String

$curr_string = '';
$curr_key=0;

$duplicate_key = array();

foreach($strings as $key=>$string) {
    $curr_key = $key;
    $curr_string = $string;
    foreach($strings as $key_ => $string_) {
        if($key_ != $key && md5($curr_string) == md5($string_)) {
            $is_exist = false;
            foreach ($duplicate_key as $dkey => $dval) {
                if($dval == $key+1 || $dval == $key_+1) {
                    $is_exist = true;
                }
            }
            if(!$is_exist) {
                array_push($duplicate_key,$key+1);
                array_push($duplicate_key,$key_+1);
            }
            
        }
    }
    if(count($duplicate_key) > 0) break;
}

if(count($duplicate_key) > 0 ) {
    foreach ($duplicate_key as $dk) {
        echo $dk."\x20";
    }
}
else {
    echo 'false';
}
