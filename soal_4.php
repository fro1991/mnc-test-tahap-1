<?php


function count_days_between_dates($start_date, $end_date) {
    $start_timestamp = strtotime($start_date);
    $end_timestamp = strtotime($end_date);
    $time_diff = abs($end_timestamp - $start_timestamp);
    $number_days = $time_diff/86400;
    return intval($number_days);
}

$total_leave_together = 7; // Input - Jumlah Cuti Bersama
$date_join = '2021-01-05'; // Input - Tanggal Join Karyawan
$req_date_leave_start = '2021-12-18'; // Input - Tanggal Rencana Cuti
$req_duration_leave = 7; // Durasi cuti (hari)

if($req_duration_leave > 3) {
    echo 'False, Durasi Cuti pribadi maksimal 3 hari';
    die;
}

$day_of_year = 365;
$total_leave_office = 14;

$date_end_year = date('Y', strtotime($date_join)).'-12-31';
$days_to_end_year = count_days_between_dates($date_join,$date_end_year);

if($days_to_end_year < 180) {
    echo 'False, Karena belum 180 hari sejak tanggal join karyawan';
    die;
}

$date_180 = date('Y-m-d',strtotime($date_join.' +180 days'));
$date180_to_end_year = count_days_between_dates($date_180,$date_end_year);
$avail_day_leave = $date180_to_end_year/$day_of_year*$total_leave_together;

if($req_duration_leave > $avail_day_leave) {
    echo 'False, Karena hanya boleh mengambil '.floor($avail_day_leave).' hari cuti';
    die;
}

echo 'True';