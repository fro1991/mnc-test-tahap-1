<?php

$total_cart = 575650; // Input - Total Belanja Seorang Customer
$buyer_pay  = 58000; // Inpuut - Pembeli Membayar

$refund_avail = array (100000,50000,20000,10000,5000,2000,1000,500,200,100); // Pecahan yang tersedia

$refund_raw = $buyer_pay-$total_cart;

if($refund_raw < 0 ) {
    echo 'False, kurang bayar';
    die;
}

echo $refund_clean = floor($refund_raw/100)*100;

$refund_cash = array();


foreach($refund_avail as $value) {
    $check = $refund_clean/$value;
    
    if((int) $check > 0) {
        $lembar = array (
            "qty" => (int) $check,
            "cash" => $value
        );
        array_push($refund_cash,$lembar);
        $refund_clean = $refund_clean - ((int) $check * $value);
    }
}

print_r($refund_cash);